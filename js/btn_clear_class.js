$(function () {

    $('.btn_clear_class').click(function(){
        $actualButton = this;
        bootbox.confirm({
            size:"extra-large",
            message: "Êtes-vous sûr de vouloir vider la classe ?<br>Tous les élèves seront placés dans la catégorie \"Sans classes\"",
            buttons: {
                confirm: {
                    label: 'Vider',
                },
                cancel: {
                    label: 'Annuler',
                }
            },
            callback: function(result){
                if(result){
                    var bodyEleve = $($actualButton).closest(".classe").find(".body_eleve");
                    bodyEleve.addClass("col-md-2").removeClass("form-group row");
                    bodyEleve.prependTo("#no_class"); //déplacer les élèves de la classe à la div no_class
                    $(".body_eleve div:first-child span").css("color","black");
                    $(".body_eleve").css("background-color","#f5f5f5");

                    $.post(
                        "./json/elv_update_cla.json.php?_="+Date.now(),
                        {
                            type:"clear_class",
                            id_cla:$($actualButton).attr('id')
                        }, function (data)
                        {

                        }
                    );

                }
                check_all_cla();
            }
        })
    });

    if($('.classe').length == 0){
        $('#content_error').html("<strong>Aucune classe correspondante</strong>");
        $('#msg_error').slideDown();
        $('#load_place').css('display','none');

    }else{
        $('#load_place').css('display','block');
        $('#msg_error').slideUp();
    }
    // console.log($('.classe').length);

});