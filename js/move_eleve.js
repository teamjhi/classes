$( function() {
    var movedEleve_id;
    var oldClasse_id;
    var newClasse_id;
    var id_elv_cla;
    var oldClasse_niv_cla;
    var oldClasse_sec_cla;

    check_all_cla();
    check_nb_elv_all_cla()

    $(".sortable").sortable({
        connectWith: ".connectedSortable",
        start: function (event, ui) {
            oldClasse_id = $(ui.item).closest(".classe").attr("id_cla");
            oldClasse_niv_cla = $(ui.item).closest(".classe").attr("niv_cla");
            oldClasse_sec_cla = $(ui.item).closest(".classe").attr("sec_cla");
        },
        stop: function (event, ui) {
            movedEleve_id = $(ui.item).attr("id_elv");
            id_elv_cla = $(ui.item).attr("id_elv_cla");
            newClasse_id = $(ui.item).closest(".eleve_container").attr("id_cla");
            if(newClasse_id !== oldClasse_id){
                check_cla(newClasse_id);
                check_cla(oldClasse_id);
                check_nb_elv(newClasse_id);
                check_nb_elv(oldClasse_id);
                if(newClasse_id==="0"){
                    // $(ui.item).attr("class", "col-md-2 body_eleve");
                    $(ui.item).removeClass("form-group");
                    $(ui.item).removeClass("row");
                    $(ui.item).addClass("col-md-2");
                    $(ui.item).attr("class", "col-md-2 body_eleve");
                    $(ui.item).css("background-color","");
                    $("#elv_"+ movedEleve_id +" div:first-child span").css("color","");
                }else{
                    // $(ui.item).attr("class", "body_eleve form-group row");
                    $(ui.item).removeClass("col-md-2");
                    $(ui.item).addClass("form-group");
                    $(ui.item).addClass("row");
                }
                // console.log(newClasse_id);
                $.post(
                    "./json/elv_update_cla.json.php?_="+Date.now(),
                    {
                        id_elv_cla:id_elv_cla,
                        id_elv:movedEleve_id,
                        id_cla:newClasse_id,
                        id_old_cla:oldClasse_id
                    }, function (data)
                    {
                        $('#elv_'+movedEleve_id).attr('id_elv_cla',data.id_elv_cla);
                    }
                );
            }
        }
    }).disableSelection();
});