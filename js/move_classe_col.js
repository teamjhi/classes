$( function() {
    $(".classe").each(function(numeroColonne){
        $(this).appendTo("#col_" + numeroColonne%6);
    });
    $(".sortable_cla").sortable({
        connectWith: ".connectedSortable_cla",
        handle: ".panel-heading"
    }).disableSelection();
} );