$(function () {
    var id_brg = $('#id_brg option:selected').attr('value');
    var id_sem = $('#id_sem option:selected').attr('value');
    var annee_elv = $('#annee_elv option:selected').attr('value');

    $('#id_brg').prop('disabled',true);


    $('#id_brg').on('change',function () {
        id_brg = $('#id_brg option:selected').attr('value');
        input_controller();
    });

    $('#id_sem').on('change',function () {
        id_sem = $('#id_sem option:selected').attr('value');
        input_controller();
    });

    $('#annee_elv').on('change',function () {
        annee_elv = $('#annee_elv option:selected').attr('value');
        input_controller();
    });

    function input_controller() {
        if(annee_elv < 9){
            $('#id_brg').prop('disabled',true);
            $('#id_brg option:first-child').prop('selected',true);
            id_brg = 0;
        }else{
            $('#id_brg').prop('disabled',false);
        }
        if(annee_elv != 0 && id_sem != 0){
            console.log("LOAD : TRUE")
            $("#load_place").load(
                "./load/classes.load.php?_="+Date.now(),
                {
                    id_brg: id_brg,
                    id_sem:id_sem,
                    annee_elv:annee_elv,
                    type:"normal"
                }
            );
            $(".all_classes").css("visibility","visible");
            $("#filtre_cla").css("visibility","visible");
            $("#show_all_classes").css("visibility","visible");
        }else{
            // console.log("LOAD : FALSE");
            $("#load_place div").remove();
            $("#load_place #msg_year_not_selected").remove();
            $("#load_place").append("<h4 id='msg_year_not_selected'>Veuillez sélectionner une année et un semestre</h4>");
            $(".all_classes").css("visibility","hidden");
            $("#filtre_cla").css("visibility","hidden");
            $("#show_all_classes").css("visibility","hidden");
        }
    }

    $('#btn_show_all_classes').on('click',function () {

        $('#load_place div').remove();
        $('#id_brg').prop('disabled',true);
        $('#annee_elv').val(0);
        $('#id_brg').val(0);
        $("#load_place").append("<div class=\"loader\"></div>");

        $("#load_place").load(
            "./load/classes.load.php?_="+Date.now(),
            {
                id_brg: id_brg,
                id_sem:id_sem,
                annee_elv:annee_elv,
                type:"all_classes"
            },function () {
                $(".all_classes").css("visibility","visible");
                $("#filtre_cla").css("visibility","visible");
                $("#show_all_classes").css("visibility","visible");
                $('#no_class div').remove();
                collapse_all_cla();
            }
        );

    })
});


