var selector;

/**
 * Fonction qui teste la nouvelle classe de l'élève bougé
 * @author M.Huber
 * @date 19.11.2019
 * @param id_cla
 */
function check_cla(id_cla) {
    var eleves = take_all_elv(id_cla);
    var needChange = false;
    for (index in eleves) {
        var tmp = eleves[index];

    //TODO : tester le degre de début et degre de fin par rapport à l'année de l'élève --> si pas similaire ou dans les bornes, utilisez la fonction UI_elv_change
        if (tmp['selected_option'] == $("#cla_" + id_cla).attr("option_0") || tmp['selected_option'] == $("#cla_" + id_cla).attr("option_1")  || tmp['selected_option'] == $("#cla_" + id_cla).attr("option_2")  || tmp['selected_option'] == " " || tmp['selected_option'] == "-") {
            UI_elv_reset(tmp['id_elv']);
        } else {
            UI_elv_change(tmp['id_elv']);
            needChange = true;
        }
    }
    UI_cla_change(id_cla,needChange);
}

// /**
//  * Fonction qui check l'ancienne classe de l'élève
//  * @author M.Huber
//  * @date 19.11.2019
//  * @param id_cla
//  */
// function check_old_cla(id_cla) {
//     var eleves = take_all_elv(id_cla);
//     var needChange = false;
//     for (index in eleves) {
//         var tmp = eleves[index];
//         if (tmp['selected_option'] == $("#cla_" + id_cla).attr("option_0") || tmp['selected_option'] == $("#cla_" + id_cla).attr("option_1")  || tmp['selected_option'] == $("#cla_" + id_cla).attr("option_2")  || tmp['selected_option'] == " " || tmp['selected_option'] == "-") {
//             UI_elv_reset(tmp['id_elv']);
//         } else {
//             UI_elv_change(tmp['id_elv']);
//             needChange = true;
//         }
//     }
//     UI_cla_change(id_cla,needChange);
// }

/**
 * Fonction qui vérifie les évèves de toutes les classes présente dans la page html
 * @author M.Huber
 * @date 19.11.2019
 */
function check_all_cla() {
    var classes = take_all_cla();
    console.log(classes);
    for(index in classes){
        var classe = classes[index];
        // console.log(classe)

        var id_cla = classe['id_cla'];
        var niv_cla = classe['niv_cla'];
        var sec_cla = classe['sec_cla'];

        if(niv_cla != undefined){
            selector = "niv_elv";
        }else if(sec_cla != undefined){
            selector = "sec_elv";
        }
        check_cla(id_cla)
    }

}


//TODO: Prendre l'annee de l'élève depuis le dom --> depuis la base (load.php)
/**
 * Fonction qui va chercher tous les élèves d'une classe(id_cla)
 * @author M.Huber
 * @date 19.11.2019
 * @param id_cla
 * @returns {any[]}
 */
function take_all_elv(id_cla) {
    var eleves = new Array();
    $('.classe_'+id_cla).find('div.body_eleve').each(function(index){
        var tmp = new Array();
        tmp['id_elv'] = $(this).attr('id_elv');
        tmp['id_elv_cla'] = $(this).attr('id_elv_cla');
        // tmp['annee_elv'] = $(this).attr('annee_elv');
        // console.log("id_tmp : "+tmp['id_elv']);
        // console.log("id_elv_cla : "+tmp['id_elv_cla']);
        tmp['selected_option'] = $(this).attr(''+selector+'');
        eleves[index] = tmp;
    });
    return eleves;
}

/**
 * Fonction qui va chercher toutes les classes depuis l'html
 * @author M.Huber
 * @date 19.11.2019
 * @returns {any[]}
 */
function take_all_cla() {
    var classes = new Array();
    $('.classe').each(function(index){
        var tmp = new Array();
        tmp['id_cla'] = $(this).attr('id_cla');
        tmp['niv_cla'] = $(this).attr('niv_cla');
        tmp['sec_cla'] = $(this).attr('sec_cla');
        tmp['degre_deb'] = $(this).attr('degre_deb');
        tmp['degre_fin'] = $(this).attr('degre_fin');
        console.log(tmp)
        classes[index] = tmp;
    });
    return classes;
}

/**
 * Fonction qui change l'affichage des classes par rapport à l'id_cla
 * @Author M.Huber
 * @date 19.11.2019
 * @param id_cla
 * @param change
 * @constructor
 */
function UI_cla_change(id_cla,change){
    if(change){
        $('div.panel_classe_'+id_cla).css('border-color','#D90000');
        $('div.panel_classe_head_'+id_cla).css('background-color','#D90000');
        $('div.panel_classe_head_'+id_cla).css('border-color','#D90000');
    }else{
        $('div.panel_classe_'+id_cla).css('border-color','');
        $('div.panel_classe_head_'+id_cla).css('background-color','').css('border-color','');
    }
}

/**
 * Fonction qui sert à changer l'affichage de l'élève séléctionné si il n'est pas dans la bonne classe
 * @author M.Huber
 * @date 19.11.2019
 * @param id_elv
 * @constructor
 */
function UI_elv_change(id_elv) {
    $('#elv_'+id_elv).css('background-color','#D90000');
    $('#elv_'+id_elv+' div:first-child span').css('color','white');
}

/**
 * Fonction qui sert à reset l'affichage de l'élève séléctionné si il est dans la bonne classe
 * @author M.Huber
 * @date 19.11.2019
 * @param id_elv
 * @constructor
 */
function UI_elv_reset(id_elv) {
    $('#elv_'+id_elv).css('background-color','');
    $('#elv_'+id_elv+' div span').css('color','');
}

function count_elv_in_cla(id_cla) {
    var nb_elv = 0;
    $('.classe_'+id_cla).find('div.body_eleve').each(function(index){
        nb_elv++;
    });
    return nb_elv;
}

function check_nb_elv(id_cla) {
    var nb_elv_cla = count_elv_in_cla(id_cla);
    $('.footer_cla_'+id_cla+' span.count_cla').html('<span>'+nb_elv_cla+' élèves</span>')
}

function check_nb_elv_all_cla() {
    var classes = take_all_cla();
    for (index in classes) {
        check_nb_elv(classes[index]['id_cla'])
    }
}
