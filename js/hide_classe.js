$( function() {
    var classeHeightArray = new Array();
    $( ".classe-toggle, .click_hide" ).on( "click", function() {
        var icon = $( this ).find(".classe-toggle");
        var nomClasse = icon.closest(".eleve_container").attr("id_cla");
        if(icon.attr('class') === 'glyphicon glyphicon-chevron-up classe-toggle') {
            $(icon).attr('class', 'glyphicon glyphicon-chevron-down classe-toggle');
            classeHeightArray[nomClasse] = icon.closest( ".eleve_container" ).find( ".panel-body" ).css("height");
            icon.closest( ".eleve_container" ).find( ".panel-body" ).animate({height:"0px","padding-top":"0px","padding-bottom":"0px"},500,function(){});
        } else {
            $(icon).attr('class', 'glyphicon glyphicon-chevron-up classe-toggle');
            icon.closest( ".eleve_container" ).find( ".panel-body" ).animate({height:classeHeightArray[nomClasse],"padding-top":"15px","padding-bottom":"15px"},500,function () {
                $(this).attr("style","");
            });
        }
    });
    var noClasseHeight = 0;
    $( ".no_classe-toggle, .click_hide_no_classe" ).on( "click", function() {
        var icon = $( this ).find(".no_classe-toggle");
        if(icon.attr('class') === 'glyphicon glyphicon-chevron-up no_classe-toggle') {
            $(icon).attr('class', 'glyphicon glyphicon-chevron-down no_classe-toggle');
            noClasseHeight = icon.closest( ".panel-warning" ).find( ".panel-body" ).css("height");
            icon.closest( ".panel-warning" ).find( ".panel-body" ).animate({height:"0px","padding-top":"0px","padding-bottom":"0px"},500,function(){});
        } else {
            $(icon).attr('class', 'glyphicon glyphicon-chevron-up no_classe-toggle');
            icon.closest( ".panel-warning" ).find( ".panel-body" ).animate({height:noClasseHeight,"padding-top":"15px","padding-bottom":"15px"},500,function () {
                $(this).attr("style","");
            });
        }
    });

    $("#collapse_all_classes").on('click', function () {
        collapse_all_cla();
    });
    $("#open_all_classes").on('click', function () {
        //console.log("test");
        open_all_cla();
    });
    $("#filtre_cla").on('keyup',function () {
        $(".panel_classe").css("display", "none");
        $(".panel-title:contains("+ $(this).val() + ")").closest(".panel_classe").css("display", "block");

    })
});

function collapse_all_cla() {
    $(".panel_classe .panel-body").css({height:0,"padding-top":"0px","padding-bottom":"0px"});
    $(".panel_classe .panel-heading .glyphicon-chevron-up").attr("class", "glyphicon glyphicon-chevron-down classe-toggle");
}

function open_all_cla() {
    $(".panel_classe .panel-body").css({
        "height":"auto",
        "padding-top":"15px",
        "padding-bottom":"15px"
    });
    $(".panel_classe .panel-heading .glyphicon-chevron-down").attr("class", "glyphicon glyphicon-chevron-up classe-toggle");
}