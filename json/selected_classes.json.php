<?php
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";

// Autoloader des classes
require_once WAY . '/class/autoloader.inc.php';

// Securité
$autorisation_str = "PER_ADM";
require WAY . '/secure.inc.php';

$per = new Personne();
$fct = new Fonction();
$dip = new Diplome();
$brn = new Branche();
$elv = new Eleve();
$cla = new Classe();
$sem = new Semestre();

//print_r($_POST);
$tab = $_POST;
$id_sem = $tab['id_sem'];
$id_brg = $tab['id_brg'];
$annee_elv = $tab['annee_elv'];



$tab_elv = array();
$tab_classes = array();
$tab_elv_classe = array();


switch ($tab['id_brg']){
    case 3:
        $tab['ini_brg'] = "A";
        break;
    case 9:
        $tab['ini_brg'] = "F";
        break;
    case 5:
        $tab['ini_brg'] = "M";
        break;
}

$tab_all_elv = $cla->get_cla_and_elv_for_org($tab['ini_brg'],$tab['id_sem'],$annee_elv);

$tab_all_classes = $cla->get_classes_brg($tab['ini_brg'],$tab['id_sem']);
//print_r($tab_all_classes[0]);
foreach ($tab_all_classes as $key_cla => $classe){
    $cla = new Classe($classe['id_cla']);
    $tab_tmp = array();
    $tab_tmp['nom'] = $cla->get_nom_cla_with_nun();
    $tab_tmp['id_cla'] = $cla->get_id();
    $tab_tmp['elv'] = $cla->get_tab_elv();
    $tab_classes[] = $tab_tmp;
    /*
    foreach ($tab_all_elv as $key_elv=>$elv){
        if($elv['annee_elv'] == $tab['id_annee_elv']){
            $tab_elv[] = $elv;
                if($classe['id_cla'] == $elv['id_cla']){
                    $tab_classes['classe_'.$classe['id_cla']]['eleves'][$elv['id_elv']] = $elv;
                }
        }
    }*/
}

//print_r($tab_all_elv[0]);
echo json_encode($tab_classes);
