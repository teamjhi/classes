<?php
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";

// Autoloader des classes
require_once WAY . '/class/autoloader.inc.php';

// Securité
$autorisation_str = "ELV_ADM";
require WAY . '/secure.inc.php';

$tab_rep = $_POST;
if($_POST['type'] == "clear_class")
{
    $cla = new Classe($_POST['id_cla']);
    $cla->del_all_elv_cla();
    $tab_rep['type'] = 'clear_class_done';
}
else
    {
    if ($_POST['id_cla'] != 0) {
        $cla = new Classe($_POST['id_cla']);

        if ($_POST['id_elv_cla'] == 0) {
            $tab_rep['id_elv_cla'] = $cla->add_elv_cla($_POST['id_elv'], $_POST['id_cla'], 0);
            $tab_rep['type'] = 'add';
        } else {
            $tab_rep['type'] = 'update';
            $tab_rep['reponse'] = $cla->update_elv_cla($_POST['id_elv_cla'], $_POST['id_cla'], $_POST['id_elv']);
        }
    } else {
        $cla = new Classe($_POST['id_old_cla']);
        $tab_rep['reponse'] = $cla->del_elv_cla($_POST['id_elv']);
        $tab_rep['type'] = 'delete';
    }
    }

$tab_rep['check_nom_cla'] = $cla->check_nom_cla();

echo json_encode($tab_rep);