<?php
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";

// Autoloader des classes
require_once WAY . '/class/autoloader.inc.php';

// Securité
$autorisation_str = "ELV_ADM";
require WAY . '/secure.inc.php';

$id_sem = $_POST['id_sem'];
$id_brg = $_POST['id_brg'];
$annee_elv = $_POST['annee_elv'];

$brn = new Branche_gen($id_brg);
$sem = new Semestre($id_sem);

//print_r($_POST);
$tab = $_POST;


$tab_elv = array();
$tab_classes = array();
$tab_elv_classe = array();


$ini_brg = strtoupper(substr($brn->get_niveau_brn(),0,1));

$cla = new Classe();
if($tab['type'] == "all_classes"){
    $tab_all_classes = $cla->get_all_cla_by_sem($id_sem);
}else {
    if ($id_brg != 0) {
        $tab_all_classes = $cla->get_classes_brg($ini_brg, $id_sem, $annee_elv);
    } else {
        $tab_all_classes = $cla->get_cla_no_brg_by_degre($annee_elv, $id_sem);
    }
}
//print_r($tab_all_classes);
foreach ($tab_all_classes as $key_cla => $classe) {
    $cla = new Classe($classe['id_cla']);
    $tab_tmp = $classe;
    $tab_tmp['nom'] = $cla->get_nom_cla_with_nun();
    //$tab_tmp['id_cla'] = $cla->get_id();
    $tab_tmp['elv'] = $cla->get_tab_elv($id_sem);
    $tab_classes[] = $tab_tmp;
}
if (!$ini_brg == 0) {
    $ini_actual;
    switch ($ini_brg) {
        case "A":
            $ini_actual = "niveau_al";
            break;
        case "F":
            $ini_actual = "niveau_fr";
            break;
        case "M":
            $ini_actual = "niveau_ma";
            break;
    }
}

//echo "<pre>";
//print_r($tab_classes);
//echo "</pre>";
?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
<div class="col-md-12 no_classe eleve_container" id_cla="0">
    <div class="panel panel-warning">
        <div class="panel-heading click_hide_no_classe">
            <span>
                <span class='glyphicon glyphicon-chevron-up no_classe-toggle'></span>
                <h3 class="panel-title">Sans classe</h3>
            </span>
        </div>
        <div class="panel-body">
            <div class="sortable connectedSortable clearfix" id="no_class">
                <?php
                $elv = new Eleve();

                if($ini_brg != ''){
                    $elv_no_cla = $elv->get_elv_no_classes_brg($ini_brg,$id_sem,$annee_elv);
                }else{
                    $elv_no_cla = $elv->get_elv_no_classes($id_sem,$annee_elv);
                }

//                echo "<pre>";
//                print_r($elv_no_cla);
//                echo "</pre>";
                foreach($elv_no_cla as $elv) :?>
                    <div id="elv_<?=$elv['id_elv']?>" class="body_eleve col-md-2" id_elv="<?=$elv['id_elv']?>" id_elv_cla="0"
                         <?php
                         if(!$ini_brg == 0){
                            echo "niv_elv=\"".$elv[$ini_actual]."\">";
                         }else{
                             echo "sec_elv=\"".$elv['section_elv']."\">";
                         }
                         ?>
                        <div class="col-md-8">
                            <span><?=$elv['nom_elv']?>, <?=$elv['prenom_elv']?></span>
                        </div>

                        <div class="col-md-2 couleur_<?= $elv[$ini_actual]?> ">
                    <span><?= $elv[$ini_actual]?>
                    </span>
                        </div>

                        <div class="col-md-2 couleur_<?=$elv['section_elv']?>">
                            <span><?=$elv['section_elv']?></span>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>

<div id="classes">
    <?php
    for($i = 0; $i < 6; $i++){
        echo "<div class='col-md-2'>";
        echo "<div class='sortable_cla connectedSortable_cla' id='col_" . $i . "'></div>";
        echo "</div>";
    }
    foreach ($tab_classes AS $cla) :?>
    <div id="cla_<?=$cla['id_cla']?>" id_cla="<?=$cla['id_cla']?>" class="classe eleve_container"
        <?php
        //TODO : Vérifier que la méthode donne les infos dont on a besoin + verificer check_classe.js quand toutes les classes sont affichées
//        echo "degre_deb=\"".$cla['degre_db_cla']."\"";
//        if($cla['degre_fin_cla'] != NULL){
//            echo "degre_fin=\"".$cla['degre_fin_cla']."\"";
//        }


        if(!$ini_brg == 0) {
            $tab_niveaux = str_split($cla['niveau_cla'], 1);
            foreach ($tab_niveaux AS $key => $niv) {
                echo " option_" . $key . "=\"" . $niv . "\" ";
            }
        }else{
            $tab_niveaux = str_split($cla['section_cla'], 1);
            foreach ($tab_niveaux AS $key => $niv) {
                echo " option_" . $key . "=\"" . $niv . "\" ";
            }
        }

        if(!$ini_brg == 0){
            echo " niv_cla=\"".$cla['niveau_cla']."\">";
        }else{
            echo " sec_cla=\"".$cla['section_cla']."\">";
        }
        ?>
        <div class="panel panel_classe panel-primary panel_classe_<?=$cla['id_cla']?>">
            <div class="panel-heading no_padding panel_classe_head_<?=$cla['id_cla']?>">
                <span class="row">
                    <span class="col-md-6 click_hide">
                        <span class='glyphicon glyphicon-chevron-up classe-toggle'></span>
                        <h3 class="panel-title"><?= $cla['nom']?></h3>
                    </span>
                    <span class="col-md-offset-3 col-md-3">
                        <button id="<?=$cla['id_cla']?>" class="btn_clear_class btn-danger btn btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                    </span>
                </span>
            </div>

            <div class="panel-body">
                    <div class="sortable connectedSortable classe_<?=$cla['id_cla']?>">
                    <?php foreach($cla['elv'] as $elv) :?>
                        <div id="elv_<?=$elv['id_elv']?>" class="body_eleve form-group row " id_elv="<?=$elv['id_elv']?>" id_elv_cla="<?=$elv['id_elv_cla']?> annee_elv="<?=$elv['annee_elv']?>"
                             <?php
                             if(!$ini_brg == 0){
                                echo "niv_elv=\"".$elv[$ini_actual]."\">";
                             }else{
                                 echo "sec_elv=\"".$elv['section_elv']."\">";
                             }
                             ?>
                            <div class="col-md-8">
                                <span><?=$elv['nom_elv']?>, <?=$elv['prenom_elv']?></span>
                            </div>

                            <div class="col-md-2 couleur_<?= $elv[$ini_actual]?> ">
                        <span><?= $elv[$ini_actual]?>
                        </span>
                            </div>

                            <div class="col-md-2 couleur_<?=$elv['section_elv']?>">
                                <span><?=$elv['section_elv']?></span>
                            </div>

                        </div>
                    <?php endforeach;?>
                </div>
            <div class="clearfix"></div>

            </div>
    <div class="panel-footer footer_cla_<?=$cla['id_cla']?>">
        <span class="count_cla"></span>
    </div>

        </div>
    </div>
    <?php endforeach;?>
</div>
<script src="js/check_classe.js?_=<?=time()?>"></script>
<script src="js/move_eleve.js?_=<?=time()?>"></script>
<script src="js/hide_classe.js?_=<?=time()?>"></script>
<script src="js/btn_clear_class.js?_=<?=time()?>"></script>
<script src="js/move_classe_col.js?_=<?=time()?>"></script>