<?php
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";

// Autoloader des classes
require_once WAY . '/class/autoloader.inc.php';

// Securité
$autorisation_str = "PER_ADM";
require WAY . '/secure.inc.php';

// Nouveau header
require(WAY . "/include/header.inc.php");

$scripts_list = [''];
require_once WAY . '/include/scripts.inc.php';

/*** NavBar *******************************************************/
$back = ROOT . "index.php?mnu=15";
require(WAY . "/include/navbar.inc.php");
/*** NavBar *******************************************************/
$classe= new Classe();
$tab_ens = $classe->get_tab_ens();

$classe = new Classe();
$tab_all_brg = $classe->get_tab_branches_generiques();

$sem = new Semestre();
$tab_sem = $sem->get_all_order_by_date();
if(!isset($_POST['id_sem'])){
    $sem->init_by_date(date("Y-m-d"));
}


?>

<html>

    <head>
        <!--script-- src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script-->
        <link rel="stylesheet" href="./css/module.css" type="text/css">
    </head>

    <body>

        <div id="brg_sem_annee" class="col-md-12">

            <div id="semestres" class="col-md-3">

                <div class="col-md-3">
                    <h4>Semestre</h4>
                </div>

                <div class="col-md-9">

                    <select id="id_sem" name="id_sem" class="form-control select_sej col-md-4 selectors">
                        <?php
                        echo "<option value='0'>&nbsp;</option>";
                        foreach ($tab_sem AS $semestre) {
                            echo "<option ";
                            if ($sem->get_id() == $semestre['id_sem']) {
                                echo " selected ";
                            }
                            $date_db = new DateTime($semestre['date_debut_sem']);
                            $date_fin = new DateTime($semestre['date_fin_sem']);
                            $date_db_ejc = new DateTime($semestre['date_debut_sej']);
                            $date_fin_ejc = new DateTime($semestre['date_fin_sej']);
                            echo "value=\"" . $semestre['id_sem'] . "\">";
                            echo $date_db->format("d.m.y") . " - " . $date_fin->format("d.m.y");
                            echo "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div id="annees" class="col-md-2">
                <div class="col-md-4">
                    <h4>Année</h4>
                </div>

                <div class="col-md-8">
                    <select id="annee_elv" class="form-control">
                        <option value="0"></option>
                        <option value="1">1H</option>
                        <option value="2">2H</option>
                        <option value="3">3H</option>
                        <option value="4">4H</option>
                        <option value="5">5H</option>
                        <option value="6">6H</option>
                        <option value="7">7H</option>
                        <option value="8">8H</option>
                        <option value="9">9H</option>
                        <option value="10">10H</option>
                        <option value="11">11H</option>
                    </select>
                </div>
            </div>

            <div id="branches" class="col-md-2">

                <div class="col-md-4">
                    <h4>Branches</h4>
                </div>

                <div class="col-md-8">
                    <select id="id_brg" class="form-control selectors">
                        <?php
                        echo "<option value='0'>&nbsp;</option>";
                        foreach($tab_all_brg as $brn){
                            if($brn['nom_brg'] == "Allemand" || $brn['nom_brg'] == "Français" ||$brn['nom_brg'] == "Mathématiques") {
                                echo "<option nom_brg=\"" . $brn['nom_brg'] . "\" value='" . $brn['id_brg'] . "'>";
                                echo $brn['nom_brg'];
                                echo "</option>\n";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>






            <div class="col-md-2 all_classes">
                <h4 id="all_classes">Toutes les classes</h4>
                <button id="collapse_all_classes" class="btn btn-primary"><span class="glyphicon glyphicon-chevron-up"></span></button>
                <button id="open_all_classes" class="btn btn-primary"><span class="glyphicon glyphicon-chevron-down"></span></button>
            </div>

            <div class="col-md-2">
                <input id="filtre_cla" type="text" class="form-control" placeholder="Filtre">
            </div>
            <div id="show_all_classes">
                <button id="btn_show_all_classes" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span></button>
            </div>

        </div>

        <div id="load_place">
            <h4 id='msg_year_not_selected'>Veuillez sélectionner une année et un semestre</h4>
        </div>

    </body>
        <script src="js/selected_test.js"></script>
</html>